package business;

import dataModel.Monomial;
import dataModel.Polynomial;

import java.util.ArrayList;
import java.util.regex.*;

public interface GUIToLogic {
    public static ArrayList<Monomial> getMonomialList(String s){
        ArrayList<Monomial> arr = new ArrayList<>(0);
        Pattern p = Pattern.compile("([-+]?)([0-9]*)?(X(\\^([0-9]*))?)?");
        // System.out.println(s1);
        String[] monomials = s.split("\\s+");
        for(String s1 : monomials){
            Matcher m = p.matcher(s1);
            if(m.matches() ) {
                if (!(s1.equals("+") || s1.equals("-"))) {
                    arr.add(new Monomial(s1));
                }
            }
            else {
                return null;
            }
            //System.out.println(s);
        }
        return arr;
    }
    public static String getResult(String s1, String s2, String option){
        String rez = "Wrong Input";
        ArrayList<Monomial> arr = new ArrayList<>(0);
        Polynomial p1,p2;
        arr = getMonomialList(s1);
        if(arr == null)
            return rez;
        else
        {
            p1 = new Polynomial(arr);
           // rez = p1.toString();
        }
        arr = getMonomialList(s2);
        if(arr == null)
            return rez;
        else
        {
            p2 = new Polynomial(arr);
            //rez = p2.toString();
        }
        if(option.toLowerCase().equals("add")){
            rez = PolynomialOperations.add(p1,p2,0).toString();
        }
        if(option.toLowerCase().equals("subtract")){
            rez = PolynomialOperations.add(p1,p2,1).toString();
        }
        if(option.toLowerCase().equals("multiply")){
            rez = PolynomialOperations.mul(p1,p2).toString();
        }
        if(option.toLowerCase().equals("divide")){
            rez ="q:";
            ArrayList<Polynomial> divRez;
            if(p1.getGrade() >= p2.getGrade())
                divRez = PolynomialOperations.divide(p1,p2);
            else
                divRez = PolynomialOperations.divide(p2,p1);
            rez += divRez.get(0).toString();
            rez += " r: " + divRez.get(1).toString();
            //rez = "Not yet implemented";
        }
        if(option.toLowerCase().equals("integrate")){
            rez = PolynomialOperations.integrate(p1).toString();
        }
        if(option.toLowerCase().equals("derivate")){
            rez = PolynomialOperations.derivate(p1).toString();
        }
        return rez;
    }
}
