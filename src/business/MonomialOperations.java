package business;

import dataModel.Monomial;

public interface MonomialOperations {
    public static Monomial div(Monomial up, Monomial down){
        Monomial rez = new Monomial(up.getPower() - down.getPower(), up.getCoef()/down.getCoef());
        return rez;
    }
}
