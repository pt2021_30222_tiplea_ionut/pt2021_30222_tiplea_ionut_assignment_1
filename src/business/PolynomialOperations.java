package business;

import dataModel.Monomial;
import dataModel.Polynomial;

import java.util.ArrayList;
import java.util.ListIterator;


public interface  PolynomialOperations {
    //add and subtract methods for Polynomial, sign = 0 => first + second , sign = 1 => first - second
    public static Polynomial add(Polynomial first, Polynomial second,int sign){
        Polynomial rez = new Polynomial(0);
        for(Monomial m : first.getArr()){
            rez.insertMonomial(new Monomial(m.getPower(),m.getCoef()));
        }
        for(Monomial m : second.getArr()){
            rez.insertMonomial(new Monomial(m.getPower(),m.getCoef()*(1-2*sign)));
        }
        rez.restructure();
        return rez;
    }
    public static Monomial lead(Polynomial p){
        int max = 0;
        Monomial rez = null;
        //creates a new monomial wich represents the leading term of polynomial p
        if(p.getArr().size() > 0)
            rez = new Monomial(p.getArr().get(p.getArr().size()-1).getPower(),p.getArr().get(p.getArr().size()-1).getCoef());
        //else
        return rez;
    }
    public static Polynomial mul(Polynomial first, Polynomial second){
        Polynomial rez = new Polynomial(0);
        for(Monomial m1 : first.getArr())
            for(Monomial m2 :second.getArr())
                rez.insertMonomial(new Monomial(m1.getPower() + m2.getPower(),m1.getCoef()*m2.getCoef()));
        rez.restructure();
        return rez;
    }
    public static Polynomial derivate(Polynomial first){
        Polynomial rez = new Polynomial(0);
        for(Monomial m:first.getArr()){
            rez.insertMonomial(new Monomial(m.getPower() -1, m.getCoef()* m.getPower()));
        }
        rez.deleteZeros();
        return rez;
    }
    public static Polynomial integrate(Polynomial first){
        Polynomial rez = new Polynomial(0);
        for(Monomial m:first.getArr()){
            rez.insertMonomial(new Monomial(m.getPower() +1, 1.0*m.getCoef()/ (m.getPower() + 1)));
        }
        return rez;
    }
    public static ArrayList<Polynomial> divide(Polynomial first, Polynomial second){
        ArrayList<Polynomial> rez = new ArrayList<>(0);
        Polynomial r,q,zero;
        zero = new Polynomial();
        r = new Polynomial(first.getArr());
        q = new Polynomial();
        if(second.equals(zero)) return null;
        while(!r.equals(zero) && r.getGrade() >= second.getGrade() && r.getArr().size()>0){
            ArrayList<Monomial> aux = new ArrayList<>(0);
            aux.add(MonomialOperations.div(lead(r),lead(second)));
            Polynomial t = new Polynomial(aux);
            q = add(q,t,0);
            r = add(r,mul(t,second),1);
        }
        rez.add(q);
        rez.add(r);
        return rez;
    }
}
