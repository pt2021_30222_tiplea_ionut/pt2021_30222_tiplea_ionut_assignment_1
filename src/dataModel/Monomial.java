package dataModel;
import java.util.regex.*;

public class Monomial implements Comparable<Monomial> {
    private int power;
    private double coef;
    public Monomial() {
        setCoef(0);
        setPower(0);
    }
    public Monomial(int power, double coef) {
        setCoef(coef);
        setPower(power);
    }
    public Monomial(String s){
        String[] myS = s.split("X");
        String[] mySs = new String[2];
        mySs[0] = myS.length > 0 ? myS[0] : "";
        mySs[1] = myS.length > 1 ? myS[1] : "";
        if(s.contains("X")){
            this.coef = mySs[0].equals("") ? 1 : Integer.parseInt(mySs[0]);
            this.power = mySs[1].equals("") ? 1 : Integer.parseInt(mySs[1].substring(1));
        }
        else
        {
            this.coef = mySs[0].equals("") ? 0 : Integer.parseInt(myS[0]);
            this.power = 0;
        }

        //2X^2 , X, X^32, 5
    }
    public int getPower() {
        return power;
    }
    //adds the monomial "other" to this monomial so it cant go to the interface MonomialOperations
    // this one is only used to restructure the Polynomial after any operation performed on it that may require it
    public void add(Monomial other, int sign) {
        if (sign != 0 && sign != 1)
            return;
        else
            this.coef += (1 - 2 * sign) * other.getCoef();
    }
    @Override
    public String toString(){
        String rez = "";
        if(coef > 0)
            rez += "+ ";
        if(coef == 1){
            if(power > 0)
                rez += "X^" + power;
            else
                rez += "1 ";
        }
        else if(coef != 0) {
            if(power > 0)
                rez += coef + "X^" + power + " ";
            else
                rez += coef + " ";
        }
        return rez;
    }
    public int compareTo(Monomial aux){
        if(aux.power > this.power)
            return -1;
        else if(aux.power < this.power)
            return 1;
        else
            return 0;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public double getCoef() {
        return coef;
    }
    public void setCoef(double coef) {
        this.coef = coef;
    }
}
