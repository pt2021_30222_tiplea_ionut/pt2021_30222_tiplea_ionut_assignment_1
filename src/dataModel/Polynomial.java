package dataModel;
import java.util.ArrayList;

public class Polynomial {
    private ArrayList<Monomial> arr;
    private int grade;
    public Polynomial (){
        arr = new ArrayList<Monomial>(0);
        grade = 0;
    }
    public Polynomial (int grade){
        arr = new ArrayList<Monomial>(grade);
        this.grade = grade;
    }
    public Polynomial(ArrayList<Monomial> myInput){
        arr = new ArrayList<Monomial>(myInput.size());
        for(Monomial e : myInput){
            arr.add(new Monomial(e.getPower(), e.getCoef()));
            //grade = grade > e.getPower() ? grade : e.getPower();
        }
        restructure();
    }
    public void deleteZeros(){
        if(arr == null) return;
        // removes those monomials from arr which have the coefficient zero
        arr.removeIf(m -> m.getCoef() == 0);
    }
    public void restructure(){
        this.arr.sort(Monomial::compareTo);
        int i = 0;
        while(i < arr.size()-1){
            if(arr.get(i).compareTo(arr.get(i+1)) == 0)
            {
                arr.get(i).add(arr.get(i+1),0);
                arr.remove(i+1);
                i --;
            }
            i ++;
        }
        deleteZeros();
        grade = 0;
        for(Monomial e : arr){
            grade = Math.max(grade,e.getPower());
        }
    }
    public void insertMonomial(Monomial other){
        arr.add(other);
    }
    public String toString(){
        String rez = "";
        for(Monomial e : arr){
            rez += e;
        }
        if(rez.isEmpty())
            rez += "0";
        return rez;
    }
    public int getGrade(){
        return this.grade;
    }
    public ArrayList<Monomial> getArr(){
        return this.arr;
    }
}
