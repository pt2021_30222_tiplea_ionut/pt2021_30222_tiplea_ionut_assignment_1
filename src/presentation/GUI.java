package presentation;

import javax.swing.*;
import java.awt.event.*;
import business.GUIToLogic;

class GUI implements ActionListener {
    private static JFrame frame;
    private static JPanel p = new JPanel();
    private static JTextField input1;
    private static JTextField input2;
    private static JTextField rez;
    private static JLabel label2;
    private static JLabel label3;
    private static JComboBox operationList;
    private static JButton button;
    public GUI(){
        frame = new JFrame("Polynomial Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,300);
        JLabel label1 = new JLabel("input1:");
        label2 = new JLabel("input2:");
        label3 = new JLabel("result");
        button = new JButton("=");
        input1  = new JTextField(20);
        input2 = new JTextField(20);
        rez    = new JTextField(20);
        rez.setVisible(false);
        label3.setVisible(false);
        String[] options = {"Add", "Subtract", "Multiply","Divide","Derivate", "Integrate"};
        operationList= new JComboBox(options);
        operationList.addActionListener(this);
        input1.addActionListener(this);
        input2.addActionListener(this);
        button.addActionListener(this);
        p.add(label1);
        p.add(input1);
        p.add(label2);
        p.add(input2);
        p.add(button);
        p.add(operationList);
        p.add(label3);
        p.add(rez);
        frame.getContentPane().add(p);
    }
    public void actionPerformed(ActionEvent e) {
        String option = (String)operationList.getSelectedItem();
        String s1 = "";
        String s2 = "";
        if(option.equals("Derivate") || option.equals( "Integrate")) {
            input2.setVisible(false);
            label2.setVisible(false);
        }
        else
        {
            input2.setVisible(true);
            label2.setVisible(true);
        }
        if(e.getSource() == button){
            //System.out.println(1);
            s1 = input1.getText();
            s2 = input2.getText();
            //rez.setText(s1 + s2);
            rez.setText( GUIToLogic.getResult(s1,s2,option) );
            label3.setVisible(true);
            rez.setVisible(true);
        }
        p.updateUI();
    }
    public static void main(String args[]){
        GUI myGUI = new GUI();
        GUI.frame.setVisible(true);
    }
}